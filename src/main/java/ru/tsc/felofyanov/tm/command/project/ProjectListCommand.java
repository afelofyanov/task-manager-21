package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getServiceLocator().getProjectService().findAll(userId, sort);

        int index = 1;
        System.out.println("[INDEX.NAME]-------------------[ID]--------------------[STATUS]--------[DATE BEGIN]");
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + "." + project);
            index++;
        }
    }
}
