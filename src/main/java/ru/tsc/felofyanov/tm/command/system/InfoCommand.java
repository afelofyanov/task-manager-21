package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.util.NumberUtil;

public final class InfoCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Show system info.";
    }

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();
        System.out.println("Available processors (cores): " + runtime.availableProcessors());

        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));

        final long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String memoryValue = isMemoryLimit ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory: " + memoryValue);

        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory in JVM: " + NumberUtil.formatBytes(usedMemory));
    }
}
