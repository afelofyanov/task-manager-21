package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");

        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }
}
