package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");

        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        System.out.println("ENTER DATE BEGIN: ");
        final Date dateBegin = TerminalUtil.nextDate();

        System.out.println("ENTER DATE END: ");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        getServiceLocator().getTaskService().create(userId, name, description, dateBegin, dateEnd);
    }
}
