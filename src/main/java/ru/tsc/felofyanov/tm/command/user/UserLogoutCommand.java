package ru.tsc.felofyanov.tm.command.user;

import ru.tsc.felofyanov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Logout of current user";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        getServiceLocator().getAuthService().logout();
    }
}
