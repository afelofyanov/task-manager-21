package ru.tsc.felofyanov.tm.command.user;

import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "View profile of current user";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[VIEW USER PROFILE]");
        final User user = getServiceLocator().getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }
}
