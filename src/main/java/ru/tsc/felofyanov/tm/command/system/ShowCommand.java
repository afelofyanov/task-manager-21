package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowCommand extends AbstractSystemCommand {
    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
}
