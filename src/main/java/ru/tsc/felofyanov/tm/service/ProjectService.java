package ru.tsc.felofyanov.tm.service;

import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.IProjectService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Project project = create(userId, name, description);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(IdEmptyException::new);
        final Project project = findOneById(id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(IdEmptyException::new);
        final Project project = findOneByIndex(index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, String id, Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Project project = findOneById(id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setUserId(userId);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, Integer index, Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        final Project project = findOneByIndex(index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setUserId(userId);
        project.setStatus(status);
        return project;
    }
}
