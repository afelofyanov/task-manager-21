package ru.tsc.felofyanov.tm;

import ru.tsc.felofyanov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}
