package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel>
        extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(user -> userId.equals(user.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return models.stream()
                .filter(user -> userId.equals(user.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return models.stream()
                .anyMatch(user -> userId.equals(user.getUserId()) && id.equals(user.getId()));
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(user -> id.equals(user.getId()) && userId.equals(user.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(models::remove);
        return model.get();
    }
}
