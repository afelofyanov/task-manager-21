package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models.stream()
                .filter(task -> projectId.equals(task.getProjectId())
                        && userId.equals(task.getUserId())
                        && task.getProjectId() != null
                        && task.getUserId() != null)
                .collect(Collectors.toList());
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }
}
