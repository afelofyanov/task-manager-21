package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return models.stream()
                .anyMatch(item -> id.equals(item.getId()));
    }

    @Override
    public M findOneById(final String id) {
        return models.stream()
                .filter(item -> id.equals(item.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M removeById(final String id) {
        final Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M removeByIndex(final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(models::remove);
        return model.get();
    }
}
