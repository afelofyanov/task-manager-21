package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IUserOwnerRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);
}
