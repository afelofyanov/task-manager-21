package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.model.User;

public interface IUserService extends IService<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String middleName, String lastName);
}
